/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.lab10part4;

/**
 *
 * @author jaiminlakhani
 */
public class CreditPaymentService extends PaymentService {

    public CreditPaymentService(double amount) {
        super(amount);
    }
    
  @Override
    public void processPayment(double amount) 
    {
        double payment = amount;
        System.out.println(" credit card payment  " +payment);
        
    }    
}
