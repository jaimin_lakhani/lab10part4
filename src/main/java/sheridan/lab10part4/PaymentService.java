/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.lab10part4;

/**
 *
 * @author jaiminlakhani
 */
public abstract class PaymentService {
    private  double amount;
   

   
    public PaymentService(double amount) {
        this.amount = amount;
    }


    public double getAmount() {
        return amount;
    }
    
    public abstract void processPayment(double amount);
    
    
}
