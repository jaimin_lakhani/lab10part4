/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.lab10part4;

/**
 *
 * @author jaiminlakhani
 */
public class ShoppingCartDemo {
    public static void main(String args[]) {
        PaymentServiceFactory factory = PaymentServiceFactory.getInstance();
        PaymentServiceType creditService = factory.getPaymentService(PaymentServiceType.DEBIT);
        PaymentServiceType debitService = factory.getPaymentService(PaymentServiceType.CREDIT);
        Cart cart = new Cart();
        cart.addProduct(new Product("Shirt", 50));
        cart.addProduct(new Product("pants",60));
        cart.setPaymentService(creditService);
        cart.payCart();
        cart.setPaymentService(debitService);
        cart.payCart();
    }
}
