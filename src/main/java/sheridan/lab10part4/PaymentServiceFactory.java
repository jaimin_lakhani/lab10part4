/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package sheridan.lab10part4;

/**
 *
 * @author jaiminlakhani
 */
public class PaymentServiceFactory {
   
    private static PaymentServiceFactory factory;
    
    
    private PaymentServiceFactory()
    {}
    
    public static PaymentServiceFactory getInstance()
    {
        if(factory==null)
            factory = new PaymentServiceFactory();
        return factory;
    }
   
    public PaymentServiceType getPaymentService(PaymentServiceType type) {
        if(type == PaymentServiceType.DEBIT)
            return PaymentServiceType.DEBIT;
        else
            return PaymentServiceType.CREDIT;
    }
}
